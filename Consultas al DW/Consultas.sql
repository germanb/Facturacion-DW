﻿--Consultas al DW
-- Venta vista por mes o por año, por sucursal, por región, por cliente y demás combinaciones entre las perspectivas.
SELECT date_part('year', v.fecha)as año, date_part('month',v.fecha)as mes, v.id_factura, cli.id_cliente, cli.nombre, p.id_producto, p.nombre, dg.descripcion, mp.descripcion, v.monto_vendido, v.cant_vendida, r.descripcion
FROM "Ventas" v, "Clientes" cli, "Tipos_Cliente" tc, "Productos" p, "Distribucion_Geografica" dg, "Ciudad" c, "Provincia" prov, "Medio_Pago" mp, "Region" r
WHERE v.id_cliente = cli.id_cliente AND v.id_producto = p.id_producto AND v.id_sucursal = dg.id_sucursal AND v.id_medio_pago = mp.id_medio_pago AND dg.id_ciudad = c.id_ciudad AND c.id_provincia = prov.id_provincia AND prov.id_region = r.id_region
GROUP BY CUBE (v.id_factura, v.cant_vendida, v.monto_vendido, (cli.id_cliente, cli.nombre),p.id_producto, p.nombre, mp.descripcion, r.descripcion, (dg.id_sucursal, dg.descripcion), año, mes);

-- Es necesario conocer también de que manera influye, en las ventas de productos,la zona geográfica en la que están ubicados los locales.
SELECT r.id_region, r.descripcion, prod.nombre, v.cant_vendida, v.monto_vendido
FROM "Region" r, "Productos" prod, "Ventas" v, "Distribucion_Geografica" dg, "Ciudad" c, "Provincia" p
WHERE v.id_sucursal = dg.id_sucursal AND dg.id_ciudad = c.id_ciudad AND c.id_provincia = p.id_provincia AND p.id_region = r.id_region AND v.id_producto = prod.id_producto
GROUP BY ROLLUP ((r.id_region, r.descripcion), prod.nombre, (v.cant_vendida, v.monto_vendido));

-- De cada cliente se desea conocer cuales son los que generan mayores ingresos a la cooperativa.
SELECT cli.id_cliente, cli.nombre, v.monto_vendido, rank() OVER (ORDER BY SUM(v.monto_vendido)desc) as posicion
FROM "Clientes" cli, "Ventas" v
WHERE v.id_cliente = cli.id_cliente 
GROUP BY cli.id_cliente, v.monto_vendido
ORDER BY posicion;

-- Se necesitará hacer análisis diarios, mensuales, trimestrales y anuales.
SELECT v.id_cliente, cli.nombre, v.id_producto, p.nombre, v.id_sucursal, v.cant_vendida, v.monto_vendido, dg.descripcion, t.
FROM "Ventas" v, "Clientes" cli, "Productos" p, "Distribucion_Geografica" dg
WHERE v.id_cliente = cli.id_cliente AND v.id_sucursal = dg.id_sucursal AND v.id_producto = p.id_producto
GROUP BY GROUPING SETS 


--Consultas al DW
-- Venta vista por mes o por año, por sucursal, por región, por cliente y demás combinaciones entre las perspectivas.
SELECT date_part('year', v.fecha)as año, date_part('month',v.fecha)as mes, v.id_factura, cli.id_cliente, cli.nombre, p.id_producto, p.nombre, dg.descripcion, mp.descripcion, v.monto_vendido, v.cant_vendida, r.descripcion
FROM "Ventas" v, "Clientes" cli, "Tipos_Cliente" tc, "Productos" p, "Distribucion_Geografica" dg, "Ciudad" c, "Provincia" prov, "Medio_Pago" mp, "Region" r
WHERE v.id_cliente = cli.id_cliente AND v.id_producto = p.id_producto AND v.id_sucursal = dg.id_sucursal AND v.id_medio_pago = mp.id_medio_pago AND dg.id_ciudad = c.id_ciudad AND c.id_provincia = prov.id_provincia AND prov.id_region = r.id_region
GROUP BY CUBE (v.id_factura, v.cant_vendida, v.monto_vendido, (cli.id_cliente, cli.nombre),p.id_producto, p.nombre, mp.descripcion, r.descripcion, (dg.id_sucursal, dg.descripcion), año, mes);

-- Es necesario conocer también de que manera influye, en las ventas de productos,la zona geográfica en la que están ubicados los locales.
SELECT r.id_region, r.descripcion, prod.nombre, v.cant_vendida, v.monto_vendido
FROM "Region" r, "Productos" prod, "Ventas" v, "Distribucion_Geografica" dg, "Ciudad" c, "Provincia" p
WHERE v.id_sucursal = dg.id_sucursal AND dg.id_ciudad = c.id_ciudad AND c.id_provincia = p.id_provincia AND p.id_region = r.id_region AND v.id_producto = prod.id_producto
GROUP BY ROLLUP ((r.id_region, r.descripcion), prod.nombre, (v.cant_vendida, v.monto_vendido));

-- De cada cliente se desea conocer cuales son los que generan mayores ingresos a la cooperativa.
SELECT cli.id_cliente, cli.nombre, v.monto_vendido, rank() OVER (ORDER BY SUM(v.monto_vendido)desc) as posicion
FROM "Clientes" cli, "Ventas" v
WHERE v.id_cliente = cli.id_cliente 
GROUP BY cli.id_cliente, v.monto_vendido
ORDER BY posicion;

-- Se necesitará hacer análisis diarios, mensuales, trimestrales y anuales.
SELECT v.id_cliente, cli.nombre, v.id_sucursal, v.id_producto, p.nombre, v.cant_vendida, v.monto_vendido, dg.descripcion, t.mes, t.año, t.trimestre
FROM "Ventas" v, "Clientes" cli, "Productos" p, "Distribucion_Geografica" dg, "Tiempo" t
WHERE v.id_cliente = cli.id_cliente AND v.id_sucursal = dg.id_sucursal AND v.id_tiempo = t.id_tiempo
GROUP BY GROUPING SETS ((v.id_cliente, cli.nombre, v.id_sucursal, v.id_producto, p.nombre, v.cant_vendida, v.monto_vendido, dg.descripcion, t.mes),
			(v.id_cliente, cli.nombre, v.id_sucursal, v.id_producto, p.nombre, v.cant_vendida, v.monto_vendido, dg.descripcion, t.año),
			(v.id_cliente, cli.nombre, v.id_sucursal, v.id_producto, p.nombre, v.cant_vendida, v.monto_vendido, dg.descripcion, t.trimestre));


