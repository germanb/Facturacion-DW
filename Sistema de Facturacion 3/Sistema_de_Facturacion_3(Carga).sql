﻿--INSERTS
--Cargar TIPOS_CLIENTES
INSERT INTO "Tipo_Cliente" VALUES (1,'Tipo_Cliente-1');
INSERT INTO "Tipo_Cliente" VALUES (2,'Tipo_Cliente-2');
INSERT INTO "Tipo_Cliente" VALUES (3,'Tipo_Cliente-3');

--Carga de CLIENTES
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (1, 'Jon Snow', 1, 'Direccion-1');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (2, 'Samwell Tarly', 2, 'Direccion-2');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (3, 'Ned Stark', 3, 'Direccion-3');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (4, 'Ian Mazzaglia', 1, 'Direccion-4');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (5, 'Emi De Marco', 2, 'Direccion-5');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (6, 'Tania Aranda', 3, 'Direccion-6');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (7, 'Arya Stark', 1, 'Direccion-7');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (8, 'Cristian Parise', 2, 'Direccion-8');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (9, 'Gabriel Ingravallo', 3, 'Direccion-9');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (10, 'Walter White', 1, 'Direccion-10');

--Cargar CATEGORIAS
INSERT INTO "Categoria" VALUES (1,'Categoria-1');
INSERT INTO "Categoria" VALUES (2,'Categoria-2');
INSERT INTO "Categoria" VALUES (3,'Categoria-3');
INSERT INTO "Categoria" VALUES (4,'Categoria-4');
INSERT INTO "Categoria" VALUES (5,'Categoria-5');
INSERT INTO "Categoria" VALUES (6,'Categoria-6');

--Carga PRODUCTOS
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(10, 'Producto-11',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(11, 'Producto-21',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(12, 'Producto-31',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(13, 'Producto-4',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(14, 'Producto-5',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(15, 'Producto-6',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(16, 'Producto-71',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(17, 'Producto-8',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(18, 'Producto-9',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(19, 'Producto-10',(floor(random()*2+1)), (5 + random()*50+1));

--Carga MEDIO_PAGO
INSERT INTO "Medio_Pago" VALUES (1, 'EFECTIVO', 3, 500, 'EFECTIVO');
INSERT INTO "Medio_Pago" VALUES (2, 'CREDITO', 12, 100, 'CREDITO');
INSERT INTO "Medio_Pago" VALUES (3, 'DEBITO', 32, 50, 'DEBITO');

--Crear Ventas
SELECT carga_ventas();
 
