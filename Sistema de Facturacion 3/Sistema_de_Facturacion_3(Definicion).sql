﻿--SISTEMA DE FACTURACION 3
CREATE DATABASE "Sistema_Facturacion_3";
--DROP DATABASE "Sistema_Facturacion_3";

--Crear tabla TIPO_CLIENTE
CREATE TABLE "Tipo_Cliente"(
	cod_tipo INT NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Tipo_Cliente"
ADD CONSTRAINT pk_Tipo_Cliente PRIMARY KEY (cod_tipo);
--DROP TABLE "Tipo_Cliente";

--Crear tabla CLIENTES
CREATE TABLE "Clientes"(
	cod_cliente INT NOT NULL,
	nombre VARCHAR(30),
	cod_tipo INT,
	direccion TEXT
);
ALTER TABLE "Clientes"
ADD CONSTRAINT pk_clientes PRIMARY KEY (cod_cliente);
ALTER TABLE "Clientes"
ADD CONSTRAINT fk_clientes FOREIGN KEY (cod_tipo) REFERENCES "Tipo_Cliente"(cod_tipo);
--DROP TABLE "Clientes";

--Crear tabla CATEGORIA
CREATE TABLE "Categoria"(
	cod_categoria INT NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Categoria"
ADD CONSTRAINT pk_categoria PRIMARY KEY (cod_categoria);
--DROP TABLE "Categoria";

--Crear tabla PRODUCTOS
CREATE TABLE "Productos"(
	cod_producto INT NOT NULL,
	nombre VARCHAR(30),
	cod_categoria INT,
	precio_actual REAL
);
ALTER TABLE "Productos"
ADD CONSTRAINT pk_productos PRIMARY KEY (cod_producto);
ALTER TABLE "Productos"
ADD CONSTRAINT fk_p_categoria FOREIGN KEY (cod_categoria) REFERENCES "Categoria"(cod_categoria);
--DROP TABLE "Productos";

--Crear tabla MEDIO_PAGO
CREATE TABLE "Medio_Pago"(
	cod_medio_pago INT NOT NULL, 
	descripcion TEXT,
	max_cuota INT,
	unidad INT,
	tipo_operacion VARCHAR(20)
);
ALTER TABLE "Medio_Pago"
ADD CONSTRAINT pk_Medio_Pago PRIMARY KEY (cod_medio_pago);
--DROP TABLE "Medio_Pago";

--Crear tabla VENTA
CREATE TABLE "Venta"(
	fecha_vta DATE,
	id_factura INT NOT NULL,
	cod_cliente INT,
	nombre VARCHAR(30),
	cod_medio_pago INT
);
ALTER TABLE "Venta"
ADD CONSTRAINT pk_venta PRIMARY KEY (id_factura);
ALTER TABLE "Venta"
ADD CONSTRAINT fk_venta_cliente FOREIGN KEY (cod_cliente) REFERENCES "Clientes"(cod_cliente);
ALTER TABLE "Venta"
ADD CONSTRAINT fk_venta_medio_pago FOREIGN KEY (cod_medio_pago) REFERENCES "Medio_Pago"(cod_medio_pago);
--DROP TABLE "Venta";

--Crear tabla DETALLE_VENTA
CREATE TABLE "Detalle_Venta"(
	id_factura INT NOT NULL,
	cod_producto INT NOT NULL,
	descripcion TEXT,
	unidad INT,
	precio REAL);
ALTER TABLE "Detalle_Venta"
ADD CONSTRAINT pk_detalle_venta PRIMARY KEY (id_factura, cod_producto);
ALTER TABLE "Detalle_Venta"
ADD CONSTRAINT fk_id_factura FOREIGN KEY (id_factura) REFERENCES "Venta"(id_factura);
ALTER TABLE "Detalle_Venta"
ADD CONSTRAINT fk_cod_producto FOREIGN KEY (cod_producto) REFERENCES "Productos"(cod_producto);
--DROP TABLE "Detalle_Venta";
