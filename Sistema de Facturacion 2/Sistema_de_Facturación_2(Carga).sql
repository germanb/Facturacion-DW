﻿--INSERTS
--Cargar TIPOS_CLIENTES
INSERT INTO "Tipo_Cliente" VALUES (1,'Tipo-1');
INSERT INTO "Tipo_Cliente" VALUES (2,'Tipo-2');
INSERT INTO "Tipo_Cliente" VALUES (3,'Tipo-3');

--Carga de CLIENTES
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (5, 'German Bianchini', 1, 'Direccion-1');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (6, 'Maxi Aguila', 2, 'Direccion-2');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (7, 'Matias Acosta', 3, 'Direccion-3');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (8, 'Tony Stark', 1, 'Direccion-4');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (9, 'Bruce Wayne', 2, 'Direccion-5');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (10, 'Peter Parker', 3, 'Direccion-6');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (11, 'Wade Wilson', 1, 'Direccion-7');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (12, 'Cristian Parise', 2, 'Direccion-8');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (13, 'Gabriel Ingravallo', 3, 'Direccion-9');
INSERT INTO "Clientes" (cod_cliente, nombre, cod_tipo, direccion) VALUES (14, 'Walter White', 1, 'Direccion-10');

--Cargar CATEGORIAS
INSERT INTO "Categoria" VALUES (1,'Categoria-1');
INSERT INTO "Categoria" VALUES (2,'Categoria-2');
INSERT INTO "Categoria" VALUES (3,'Categoria-3');
INSERT INTO "Categoria" VALUES (4,'Categoria-4');
INSERT INTO "Categoria" VALUES (5,'Categoria-5');

--Carga PRODUCTOS
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria ,precio_actual) VALUES(5, 'Producto-1',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(6, 'Producto-2',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(7, 'Producto-3',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(8, 'Producto-40',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(9, 'Producto-50',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(10, 'Producto-60',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(11, 'Producto-70',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(12, 'Producto-8',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(13, 'Producto-9',(floor(random()*2+1)), (5 + random()*50+1));
INSERT INTO "Productos" (cod_producto, nombre, cod_categoria, precio_actual) VALUES(14, 'Producto-10',(floor(random()*2+1)), (5 + random()*50+1));

--Carga MEDIO_PAGO
INSERT INTO "Medio_Pago" VALUES (1, 'EFECTIVO', 3, 500, 'EFECTIVO');
INSERT INTO "Medio_Pago" VALUES (2, 'DEBITO', 12, 100, 'DEBITO');
INSERT INTO "Medio_Pago" VALUES (3, 'CREDITO', 32, 50, 'CREDITO');

--Crear Ventas
SELECT carga_ventas(); 
