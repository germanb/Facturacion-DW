--Crear Ventas
CREATE OR REPLACE FUNCTION carga_ventas() RETURNS integer AS $$
DECLARE
	i integer := 1;
	j integer := 1;
	k integer := 1;
	l integer := 0;
	meses integer[] := array[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	fecha_base DATE := '2009-01-01';
	fecha_venta DATE;
BEGIN
	LOOP
		FOREACH i IN ARRAY meses LOOP
			FOR j IN 1..15 LOOP
				fecha_venta := fecha_base + CAST(floor(random() * i) AS INTEGER);  -- Calcula la fecha random.

				-- Condición de parada.
				EXIT WHEN fecha_venta >= current_date;

				DECLARE -- Variables Locales
					num_cliente integer := (SELECT nro_cliente FROM "Clientes" ORDER BY random() LIMIT 1);
					nombre_cliente VARCHAR := (SELECT nombre FROM "Clientes" c WHERE c.nro_cliente = num_cliente);
					f_pago VARCHAR[] := array['EFECTIVO', 'CREDITO', 'DEBITO'];

				BEGIN
					l := l + 1;
					INSERT INTO "Venta" VALUES(fecha_venta, l, num_cliente, nombre_cliente, f_pago[CAST(floor(random() * 3 + 1) AS INTEGER)]);

					DECLARE
						k_rand integer := CAST(floor(random() * 5 + 1) AS INTEGER);
						unidad integer;
						producto integer;
						precio_total real;

					BEGIN
						-- Insertamos los detalles.
						FOR k IN 1..k_rand LOOP	
							unidad := k_rand + 1;
							producto := (SELECT nro_producto 
								     FROM "Productos" p 
								     WHERE p.nro_producto NOT IN (SELECT nro_producto 
												  FROM "Detalle_Venta" 
												  WHERE nro_factura = l)
								     ORDER BY random() 
								     LIMIT 1 
								     );
							precio_total := (SELECT precio_actual FROM "Productos" p WHERE p.nro_producto = producto) * unidad;

							INSERT INTO "Detalle_Venta" VALUES(l, producto, 'Detalle'||l, unidad, precio_total);
						END LOOP;
					END;

					fecha_venta := fecha_base;  -- Reinicia la fecha base del mes para el proximo calculo.
				END;
			END LOOP;

			-- Condición de parada.
			EXIT WHEN fecha_venta >= current_date;

			fecha_base := fecha_base + i;  -- Cambia al mes siguiente.
		END LOOP;

		-- Condición de parada.
		EXIT WHEN fecha_venta >= current_date;
	END LOOP;

	RETURN 0;
END;
$$ LANGUAGE plpgsql; 
