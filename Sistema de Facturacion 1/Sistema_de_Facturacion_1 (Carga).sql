﻿--INSERTS--
--Carga de CLIENTES
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (1, 'German Bianchini', 1, 'Direccion-1');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (2, 'Maxi Aguila', 2, 'Direccion-2');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (3, 'Matias Acosta', 3, 'Direccion-3');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (4, 'Ian Mazzaglia', 1, 'Direccion-4');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (5, 'Emi De Marco', 2, 'Direccion-5');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (6, 'Tania Aranda', 3, 'Direccion-6');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (7, 'Victor Abitu', 1, 'Direccion-7');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (8, 'Cristian Parise', 2, 'Direccion-8');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (9, 'Gabriel Ingravallo', 3, 'Direccion-9');
INSERT INTO "Clientes" (nro_cliente, nombre, tipo, direccion) VALUES (10, 'Walter White', 1, 'Direccion-10');

--Carga CATEGORIAS
INSERT INTO "Categoria" VALUES (1, 'Categoria 1');
INSERT INTO "Categoria" VALUES (2, 'Categoria 2');
INSERT INTO "Categoria" VALUES (3, 'Categoria 3');
INSERT INTO "Categoria" VALUES (4, 'Categoria 4');
INSERT INTO "Categoria" VALUES (5, 'Categoria 5');

--Carga PRODUCTOS
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(1, 'Producto-1', 1, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(2, 'Producto-2', 2, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(3, 'Producto-3', 3, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(4, 'Producto-4', 4, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(5, 'Producto-5', 5, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(6, 'Producto-6', 1, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(7, 'Producto-7', 2, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(8, 'Producto-8', 3, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(9, 'Producto-9', 4, (5 + random()*50+1));
INSERT INTO "Productos" (nro_producto, nombre, nro_categ, precio_actual) VALUES(10, 'Producto-10', 5, (5 + random()*50+1));

SELECT carga_ventas(); 
