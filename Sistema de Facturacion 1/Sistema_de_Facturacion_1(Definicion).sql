﻿--SISTEMA DE FACTURACION 1
CREATE DATABASE "Sistema_Facturacion_1";
--DROP DATABASE "Sistema_Facturacion_1";

--Crear tabla CLIENTES
CREATE TABLE "Clientes"(
	nro_cliente INT NOT NULL,
	nombre VARCHAR(30),
	tipo VARCHAR(30),
	direccion VARCHAR(35)
);
ALTER TABLE "Clientes"
ADD CONSTRAINT pk_cliente PRIMARY KEY (nro_cliente);
--DROP TABLE "Clientes" CASCADE;

--Crear tabla Categoria
CREATE TABLE "Categoria"(
	nro_categoria INT NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Categoria"
ADD CONSTRAINT pk_categoria PRIMARY KEY (nro_categoria);
--DROP TABLE "Categoria";

--Crear tabla PRODUCTOS
CREATE TABLE "Productos"(
	nro_producto INT NOT NULL,
	nombre VARCHAR(30),
	nro_categ INT,
	precio_actual REAL
);
ALTER TABLE "Productos"
ADD CONSTRAINT pk_producto PRIMARY KEY (nro_producto);
ALTER TABLE "Productos"
ADD CONSTRAINT fk_producto FOREIGN KEY (nro_categ) REFERENCES "Categoria"(nro_categoria);
--DROP TABLE "Productos" CASCADE;

--Crear tabla VENTA
CREATE TABLE "Venta"(
	fecha_venta DATE,
	nro_factura INT NOT NULL,
	nro_cliente INT,
	nombre VARCHAR(30),
	forma_pago VARCHAR(30)
);
ALTER TABLE "Venta"
ADD CONSTRAINT pk_venta PRIMARY KEY (nro_factura);
ALTER TABLE "Venta"
ADD CONSTRAINT fk_venta FOREIGN KEY (nro_cliente) REFERENCES "Clientes"(nro_cliente);
--DROP TABLE "Venta" CASCADE;

--Crear tabla DETALLE VENTA
CREATE TABLE "Detalle_Venta"(
	nro_factura INT NOT NULL,
	nro_producto INT NOT NULL,
	descripcion TEXT,
	unidad INT,
	precio REAL
);
ALTER TABLE "Detalle_Venta"
ADD CONSTRAINT pk_Detalle_Venta PRIMARY KEY (nro_factura, nro_producto);
ALTER TABLE "Detalle_Venta"
ADD CONSTRAINT fk_nro_factura FOREIGN KEY (nro_factura) REFERENCES "Venta"(nro_factura);
ALTER TABLE "Detalle_Venta"
ADD CONSTRAINT fk_nro_producto FOREIGN KEY (nro_producto) REFERENCES "Productos"(nro_producto);
--DROP TABLE "Detalle_Venta";
