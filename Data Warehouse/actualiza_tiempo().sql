﻿CREATE OR REPLACE FUNCTION actualiza_tiempo(fecha DATE) RETURNS INT AS $$
DECLARE
    m INT := date_part('Month', fecha);
    a INT := date_part('Year', fecha);
    tri INT;

BEGIN
    IF m >= 1 AND m <= 3 THEN
        tri := 1;
    ELSIF m >= 4 AND m <= 6 THEN
        tri := 2;
    ELSIF m >= 7 AND m <= 9 THEN
        tri := 3;
    ELSE
        tri := 4;
    END IF;

    IF (SELECT mes FROM "Tiempo" WHERE "Tiempo".mes = m AND "Tiempo"."año" = a) IS NULL THEN
        INSERT INTO "Tiempo"(mes, "año", trimestre) VALUES (m, a, tri);
    END IF;

    RETURN (SELECT id_tiempo FROM "Tiempo" WHERE "Tiempo".mes = m AND "Tiempo"."año" = a AND "Tiempo".trimestre = tri);
END;
$$ LANGUAGE plpgsql;
