﻿CREATE OR REPLACE FUNCTION carga_equivalencia_productos() RETURNS void AS $$
DECLARE
	cursor_1 refcursor;
	cursor_2 refcursor;
	cursor_3 refcursor;
	reg_1 RECORD;
	reg_2 RECORD;
	reg_3 RECORD;
BEGIN
	-- Crea tablas temporales para el volcado de datos remotos.
	CREATE TEMP TABLE productos_temp_1(id_prod INT CONSTRAINT pk_temp_1 PRIMARY KEY, nombre VARCHAR(30), id_cat INT);
	CREATE TEMP TABLE productos_temp_2(id_prod INT CONSTRAINT pk_temp_2 PRIMARY KEY, nombre VARCHAR(30), id_cat INT);
	CREATE TEMP TABLE productos_temp_3(id_prod INT CONSTRAINT pk_temp_3 PRIMARY KEY, nombre VARCHAR(30), id_cat INT);

	-- Crea las conexiones.
	PERFORM dblink_connect('db1', 'dbname=Sistema_Facturacion_1');
	PERFORM dblink_connect('db2', 'dbname=Sistema_Facturacion_2');
	PERFORM dblink_connect('db3', 'dbname=Sistema_Facturacion_3');

	-- Completa las tablas temporales.
	INSERT INTO productos_temp_1
	SELECT *
	FROM dblink('db1', 'SELECT nro_producto, nombre, nro_categ FROM "Productos"')
		as t(id_prod INT, nombre VARCHAR(30), id_cat INT);
	INSERT INTO productos_temp_2
	SELECT *
	FROM dblink('db2', 'SELECT cod_producto, nombre, cod_categoria FROM "Productos"')
		as t(id_prod INT, nombre VARCHAR(30), id_cat INT);
	INSERT INTO productos_temp_3
	SELECT *
	FROM dblink('db3', 'SELECT cod_producto, nombre, cod_categoria FROM "Productos"')
		as t(id_prod INT, nombre VARCHAR(30), id_cat INT);

	-- Cierra las conexiones.
	PERFORM dblink_disconnect('db1');
	PERFORM dblink_disconnect('db2');
	PERFORM dblink_disconnect('db3');
	
	----------------------------------------------------------
	-- Proceso la columna 1.
	----------------------------------------------------------
	OPEN cursor_1 FOR SELECT * FROM productos_temp_1;
	LOOP
		FETCH cursor_1 INTO reg_1;
		EXIT WHEN NOT FOUND;

		-- Si no esa en la columna 1 lo inserto.
		IF reg_1.id_prod NOT IN (SELECT id_producto_sis_1 FROM "Equivalencia_Productos" WHERE id_producto_sis_1 IS NOT NULL) THEN
			INSERT INTO "Equivalencia_Productos" (id_producto_sis_1) VALUES (reg_1.id_prod);
		END IF;

		-- Si esta en la tabla 2 lo inserto en la columna 2.
		OPEN cursor_2 FOR SELECT * FROM productos_temp_2;
		LOOP
			FETCH cursor_2 INTO reg_2;
			EXIT WHEN NOT FOUND;

			IF reg_1.nombre SIMILAR TO reg_2.nombre THEN
				IF reg_1.id_cat = reg_2.id_cat THEN
					UPDATE "Equivalencia_Productos"
					SET id_producto_sis_2 = reg_2.id_prod
					WHERE id_producto_sis_1 = reg_1.id_prod;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_2;

		-- Si esta en la tabla 3 lo inserto en la columna 3.
		OPEN cursor_3 FOR SELECT * FROM productos_temp_3;
		LOOP
			FETCH cursor_3 INTO reg_3;
			EXIT WHEN NOT FOUND;

			IF reg_1.nombre SIMILAR TO reg_3.nombre THEN
				IF reg_1.id_cat = reg_3.id_cat THEN
					UPDATE "Equivalencia_Productos"
					SET id_producto_sis_3 = reg_3.id_prod
					WHERE id_producto_sis_1 = reg_1.id_prod;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_3;
	END LOOP;
	CLOSE cursor_1;

	----------------------------------------------------------
	-- Procesamos la columna 2.
	----------------------------------------------------------
	OPEN cursor_2 FOR SELECT * FROM productos_temp_2;
	LOOP
		FETCH cursor_2 INTO reg_2;
		EXIT WHEN NOT FOUND;

		-- Si no esta en la columna 2 lo inserto.
		IF reg_2.id_prod NOT IN (SELECT id_producto_sis_2 FROM "Equivalencia_Productos" WHERE id_producto_sis_2 IS NOT NULL) THEN
			INSERT INTO "Equivalencia_Productos" (id_producto_sis_2) VALUES (reg_2.id_prod);
		END IF;

		-- Si esta en la tabla 1 lo inserto en la columna 1.
		OPEN cursor_1 FOR SELECT * FROM productos_temp_1;
		LOOP
			FETCH cursor_1 INTO reg_1;
			EXIT WHEN NOT FOUND;

			IF reg_2.nombre SIMILAR TO reg_1.nombre THEN
				IF reg_2.id_cat = reg_1.id_cat THEN
					UPDATE "Equivalencia_Productos"
					SET id_producto_sis_1 = reg_1.id_prod
					WHERE id_producto_sis_2 = reg_2.id_prod;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_1;

		-- Si esta en la tabla 3 lo inserto en la columna 3.
		OPEN cursor_3 FOR SELECT * FROM productos_temp_3;
		LOOP
			FETCH cursor_3 INTO reg_1;
			EXIT WHEN NOT FOUND;

			IF reg_2.nombre SIMILAR TO reg_3.nombre THEN
				IF reg_2.id_cat = reg_3.id_cat THEN
					UPDATE "Equivalencia_Productos"
					SET id_producto_sis_3 = reg_3.id_prod
					WHERE id_producto_sis_2 = reg_2.id_prod;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_3;

	END LOOP;
	CLOSE cursor_2;

	----------------------------------------------------------
	-- Procesamos la columna 3.
	----------------------------------------------------------
	OPEN cursor_3 FOR SELECT * FROM productos_temp_3;
	LOOP
		FETCH cursor_3 INTO reg_3;
		EXIT WHEN NOT FOUND;

		-- Si no esta en la columna 2 lo inserto.
		IF reg_3.id_prod NOT IN (SELECT id_producto_sis_3 FROM "Equivalencia_Productos" WHERE id_producto_sis_3 IS NOT NULL) THEN
			INSERT INTO "Equivalencia_Productos" (id_producto_sis_3) VALUES (reg_3.id_prod);
		END IF;

		-- Si esta en la tabla 1 lo inserto en la columna 1.
		OPEN cursor_1 FOR SELECT * FROM productos_temp_1;
		LOOP
			FETCH cursor_1 INTO reg_1;
			EXIT WHEN NOT FOUND;

			IF reg_3.nombre SIMILAR TO reg_1.nombre THEN
				IF reg_3.id_cat = reg_1.id_cat THEN
					UPDATE "Equivalencia_Productos"
					SET id_producto_sis_1 = reg_1.id_prod
					WHERE id_producto_sis_3 = reg_3.id_prod;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_1;

		-- Si esta en la tabla 2 lo inserto en la columna 2.
		OPEN cursor_2 FOR SELECT * FROM productos_temp_2;
		LOOP
			FETCH cursor_2 INTO reg_2;
			EXIT WHEN NOT FOUND;

			IF reg_3.nombre SIMILAR TO reg_2.nombre THEN
				IF reg_3.id_cat = reg_2.id_cat THEN
					UPDATE "Equivalencia_Productos"
					SET id_producto_sis_2 = reg_2.id_prod
					WHERE id_producto_sis_3 = reg_3.id_prod;

					EXIT;
				END IF;

			END IF;
		END LOOP;
		CLOSE cursor_2;

	END LOOP;
	CLOSE cursor_3;

	DROP TABLE productos_temp_1;
	DROP TABLE productos_temp_2;
	DROP TABLE productos_temp_3;
END;
$$ LANGUAGE plpgsql;
