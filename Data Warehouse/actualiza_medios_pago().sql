﻿CREATE OR REPLACE FUNCTION actualiza_medios_pago(medio TEXT) RETURNS INT AS $$
DECLARE
BEGIN
    IF (SELECT descripcion FROM "Medio_Pago" WHERE descripcion SIMILAR TO medio) IS NULL THEN
        INSERT INTO "Medio_Pago"(descripcion) VALUES (medio);
    END IF;

    RETURN (SELECT id_medio_pago FROM "Medio_Pago" WHERE descripcion SIMILAR TO medio);
END;
$$ LANGUAGE plpgsql;
