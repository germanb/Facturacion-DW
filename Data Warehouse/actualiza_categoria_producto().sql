﻿CREATE OR REPLACE FUNCTION actualiza_categoria_producto(cat TEXT) RETURNS INT AS $$
DECLARE
BEGIN
    IF (SELECT descripcion FROM "Categoria" WHERE descripcion SIMILAR TO cat) IS NULL THEN
        INSERT INTO "Categoria"(descripcion) VALUES (cat);
    END IF;

    RETURN (SELECT id_categoria FROM "Categoria" WHERE descripcion = cat);
END;
$$ LANGUAGE plpgsql;
