﻿CREATE OR REPLACE FUNCTION actualiza_tipo_cliente(tipo TEXT) RETURNS INT AS $$
DECLARE
    cod_tipo INT;
BEGIN
    cod_tipo := (SELECT id_tipo FROM "Tipos_Cliente" WHERE descripcion SIMILAR TO tipo);

    IF cod_tipo IS NULL THEN
        INSERT INTO "Tipos_Cliente" (descripcion) VALUES (tipo);
        cod_tipo := (SELECT id_tipo FROM "Tipos_Cliente" WHERE descripcion = tipo);
    END IF;

    RETURN cod_tipo;
END;
$$ LANGUAGE plpgsql;
