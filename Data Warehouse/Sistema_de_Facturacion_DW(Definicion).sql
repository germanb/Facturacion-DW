﻿-- Creamos el DW
CREATE DATABASE "Sistema_Facturacion_DW";
-- DROP DATABASE "Sistema_Facturacion_DW";

-- Creamos las Tablas de Dimensiones
CREATE TABLE "Categoria"(
	id_categoria SERIAL NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Categoria"
ADD CONSTRAINT pk_categoria PRIMARY KEY (id_categoria);
-- DROP TABLE "Categoria" CASCADE;

CREATE TABLE "Productos"(
	id_producto INT NOT NULL,
	nombre VARCHAR(30),
	id_categoria INT
);
ALTER TABLE "Productos"
ADD CONSTRAINT pk_productos PRIMARY KEY(id_producto);
ALTER TABLE "Productos"
ADD CONSTRAINT fk_categoria FOREIGN KEY(id_categoria) REFERENCES "Categoria" (id_categoria);
-- DROP TABLE "Productos" CASCADE;

CREATE TABLE "Tipos_Cliente"(
	id_tipo SERIAL NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Tipos_Cliente"
ADD CONSTRAINT pk_tipo_cliente PRIMARY KEY (id_tipo);
-- DROP TABLE "Tipos_Cliente";

CREATE TABLE "Clientes"(
	id_cliente INT NOT NULL,
	nombre VARCHAR(30),
	id_tipo INT
);
ALTER TABLE "Clientes"
ADD CONSTRAINT pk_clientes PRIMARY KEY(id_cliente);
ALTER TABLE "Clientes"
ADD CONSTRAINT pf_tipo_cliente FOREIGN KEY(id_tipo) REFERENCES "Tipos_Cliente"(id_tipo);
-- DROP TABLE "Clientes";

CREATE TABLE "Tiempo"(
	id_tiempo SERIAL NOT NULL,
	mes INT,
	trimestre INT,
	año INT
);
ALTER TABLE "Tiempo"
ADD CONSTRAINT pk_tiempo PRIMARY KEY (id_tiempo);
-- DROP TABLE "Tiempo";

CREATE TABLE "Medio_Pago"(
	id_medio_pago SERIAL NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Medio_Pago"
ADD CONSTRAINT pk_medio_pago PRIMARY KEY (id_medio_pago);
-- DROP TABLE "Medio_Pago";

CREATE TABLE "Region"(
	id_region INT NOT NULL,
	descripcion TEXT
);
ALTER TABLE "Region"
ADD CONSTRAINT pk_region PRIMARY KEY (id_region);
-- DROP TABLE "Region";

CREATE TABLE "Provincia"(
	id_provincia INT NOT NULL,
	descripcion TEXT,
	id_region INT
);
ALTER TABLE "Provincia"
ADD CONSTRAINT pk_provincia PRIMARY KEY (id_provincia);
ALTER TABLE "Provincia"
ADD CONSTRAINT fk_provincia FOREIGN KEY (id_region) REFERENCES "Region" (id_region);
-- DROP TABLE "Provincia";

CREATE TABLE "Ciudad"(
	id_ciudad INT NOT NULL,
	descripcion TEXT,
	id_provincia INT
);
ALTER TABLE "Ciudad"
ADD CONSTRAINT pk_ciudad PRIMARY KEY (id_ciudad);
ALTER TABLE "Ciudad"
ADD CONSTRAINT fk_ciudad FOREIGN KEY (id_provincia) REFERENCES "Provincia" (id_provincia);
-- DROP TABLE "Ciudad";

CREATE TABLE "Distribucion_Geografica"(
	id_sucursal INT NOT NULL,
	descripcion TEXT,
	id_ciudad INT
);
ALTER TABLE "Distribucion_Geografica"
ADD CONSTRAINT pk_distribucion_geografica PRIMARY KEY (id_sucursal);
ALTER TABLE "Distribucion_Geografica"
ADD CONSTRAINT fk_distribucion_geografica FOREIGN KEY (id_ciudad) REFERENCES "Ciudad" (id_ciudad);
-- DROP TABLE "Distribucion_Geografica";

-- Creamos la Tabla de Hecho
CREATE TABLE "Ventas"(
	fecha DATE,
	id_factura INT NOT NULL,
	id_cliente INT NOT NULL,
	id_producto INT NOT NULL,
	id_sucursal INT NOT NULL,
	id_medio_pago INT NOT NULL,
	id_tiempo INT NOT NULL,
	monto_vendido REAL,
	cant_vendida INT
);
ALTER TABLE "Ventas"
ADD CONSTRAINT pk_ventas PRIMARY KEY (id_factura, id_cliente, id_producto, id_sucursal, id_medio_pago, id_tiempo);
ALTER TABLE "Ventas"
ADD CONSTRAINT fk_ventas_cliente FOREIGN KEY (id_cliente) REFERENCES "Clientes" (id_cliente);
ALTER TABLE "Ventas"
ADD CONSTRAINT fk_ventas_producto FOREIGN KEY (id_producto) REFERENCES "Productos" (id_producto);
ALTER TABLE "Ventas"
ADD CONSTRAINT fk_ventas_sucursal FOREIGN KEY (id_sucursal) REFERENCES "Distribucion_Geografica" (id_sucursal);
ALTER TABLE "Ventas"
ADD CONSTRAINT fk_ventas_medio_pago FOREIGN KEY (id_medio_pago) REFERENCES "Medio_Pago" (id_medio_pago);
ALTER TABLE "Ventas"
ADD CONSTRAINT fk_ventas_tiempo FOREIGN KEY (id_tiempo) REFERENCES "Tiempo" (id_tiempo);
-- DROP TABLE "Ventas";

-- Creamos las Tablas de Equivalencias
CREATE TABLE "Equivalencia_Productos"(
	id_producto_equiv SERIAL,
	id_producto_sis_1 INT,
	id_producto_sis_2 INT,
	id_producto_sis_3 INT
);
ALTER TABLE "Equivalencia_Productos"
ADD CONSTRAINT pk_equivalencia_productos PRIMARY KEY (id_producto_equiv);
--DROP TABLE "Equivalencia_Productos";

CREATE TABLE "Equivalencia_Clientes"(
	cod_cliente_equiv SERIAL,
	cod_cliente_sis_1 INT,
	cod_cliente_sis_2 INT,
	cod_cliente_sis_3 INT
);
ALTER TABLE "Equivalencia_Clientes"
ADD CONSTRAINT pk_equivalencia_clientes PRIMARY KEY (cod_cliente_equiv);
--DROP TABLE "Equivalencia_Clientes";
