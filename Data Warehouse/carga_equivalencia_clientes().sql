﻿CREATE OR REPLACE FUNCTION carga_equivalencia_clientes() RETURNS void AS $$
DECLARE
	cursor_1 refcursor;
	cursor_2 refcursor;
	cursor_3 refcursor;
	reg_1 RECORD;
	reg_2 RECORD;
	reg_3 RECORD;
BEGIN
	-- Crea tablas temporales para el volcado de datos remotos.
	CREATE TEMP TABLE clientes_temp_1(nro_cli INT CONSTRAINT pk_temp_1 PRIMARY KEY, nombre VARCHAR(30), direccion VARCHAR(30));
	CREATE TEMP TABLE clientes_temp_2(nro_cli INT CONSTRAINT pk_temp_2 PRIMARY KEY, nombre VARCHAR(30), direccion VARCHAR(30));
	CREATE TEMP TABLE clientes_temp_3(nro_cli INT CONSTRAINT pk_temp_3 PRIMARY KEY, nombre VARCHAR(30), direccion VARCHAR(30));

	-- Crea las conexiones.
	PERFORM dblink_connect('db1', 'dbname=Sistema_Facturacion_1');
	PERFORM dblink_connect('db2', 'dbname=Sistema_Facturacion_2');
	PERFORM dblink_connect('db3', 'dbname=Sistema_Facturacion_3');

	-- Completa las tablas temporales.
	INSERT INTO clientes_temp_1
	SELECT *
	FROM dblink('db1', 'SELECT nro_cliente, nombre, direccion FROM "Clientes"')
		as t(num int, nom VARCHAR(30), direccion VARCHAR(30));
	INSERT INTO clientes_temp_2
	SELECT *
	FROM dblink('db2', 'SELECT cod_cliente, nombre, direccion FROM "Clientes"')
		as t(num int, nom VARCHAR(30), direccion VARCHAR(30));
	INSERT INTO clientes_temp_3
	SELECT *
	FROM dblink('db3', 'SELECT cod_cliente, nombre, direccion FROM "Clientes"')
		as t(num int, nom VARCHAR(30), direccion VARCHAR(30));

	-- Cierra las conexiones.
	PERFORM dblink_disconnect('db1');
	PERFORM dblink_disconnect('db2');
	PERFORM dblink_disconnect('db3');

	----------------------------------------------------------
	-- Proceso la columna 1.
	----------------------------------------------------------
	OPEN cursor_1 FOR SELECT * FROM clientes_temp_1;
	LOOP
		FETCH cursor_1 INTO reg_1;
		EXIT WHEN NOT FOUND;

		-- Si no esta en la columna 1 lo inserto.
		IF reg_1.nro_cli NOT IN (SELECT cod_cliente_sis_1 FROM "Equivalencia_Clientes" WHERE cod_cliente_sis_1 IS NOT NULL) THEN
			INSERT INTO "Equivalencia_Clientes" (cod_cliente_sis_1) VALUES (reg_1.nro_cli);
		END IF;

		-- Si esta en la tabla 2 lo inserto.
		OPEN cursor_2 FOR SELECT * FROM clientes_temp_2;
		LOOP
			FETCH cursor_2 INTO reg_2;
			EXIT WHEN NOT FOUND;

			IF reg_1.nombre SIMILAR TO reg_2.nombre THEN
				IF reg_1.direccion SIMILAR TO reg_2.direccion THEN
					UPDATE "Equivalencia_Clientes"
					SET cod_cliente_sis_2 = reg_2.nro_cli
					WHERE cod_cliente_sis_1 = reg_1.nro_cli;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_2;

		-- Si esta en la tabla 3 lo inserto.
		OPEN cursor_3 FOR SELECT * FROM clientes_temp_3;
		LOOP
			FETCH cursor_3 INTO reg_3;
			EXIT WHEN NOT FOUND;

			IF reg_1.nombre SIMILAR TO reg_3.nombre THEN
				IF reg_1.direccion SIMILAR TO reg_3.direccion THEN
					UPDATE "Equivalencia_Clientes"
					SET cod_cliente_sis_3 = reg_3.nro_cli
					WHERE cod_cliente_sis_1 = reg_1.nro_cli;

					EXIT;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_3;

	END LOOP;
	CLOSE cursor_1;

	----------------------------------------------------------
	-- Procesamos la columna 2.
	----------------------------------------------------------
	OPEN cursor_2 FOR SELECT * FROM clientes_temp_2;
	LOOP
		FETCH cursor_2 INTO reg_2;
		EXIT WHEN NOT FOUND;

		-- Si no esta en la columna 2 lo inserto.
		IF reg_2.nro_cli NOT IN (SELECT cod_cliente_sis_2 FROM "Equivalencia_Clientes" WHERE cod_cliente_sis_2 IS NOT NULL) THEN
			INSERT INTO "Equivalencia_Clientes" (cod_cliente_sis_2) VALUES (reg_2.nro_cli);
		END IF;

		-- Si esta en la tabla 1 lo inserto en la columna 1.
		OPEN cursor_1 FOR SELECT * FROM clientes_temp_1;
		LOOP
			FETCH cursor_1 INTO reg_1;
			EXIT WHEN NOT FOUND;

			IF reg_2.nombre SIMILAR TO reg_1.nombre THEN
				IF reg_2.direccion SIMILAR TO reg_1.direccion THEN
					UPDATE "Equivalencia_Clientes"
					SET cod_cliente_sis_1 = reg_1.nro_cli
					WHERE cod_cliente_sis_2 = reg_2.nro_cli;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_1;

		-- Si esta en la tabla 3 lo inserto en la columna 3.
		OPEN cursor_3 FOR SELECT * FROM clientes_temp_3;
		LOOP
			FETCH cursor_3 INTO reg_1;
			EXIT WHEN NOT FOUND;

			IF reg_2.nombre SIMILAR TO reg_3.nombre THEN
				IF reg_2.direccion SIMILAR TO reg_3.direccion THEN
					UPDATE "Equivalencia_Clientes"
					SET cod_cliente_sis_3 = reg_3.nro_cli
					WHERE cod_cliente_sis_2 = reg_2.nro_cli;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_3;

	END LOOP;
	CLOSE cursor_2;

	----------------------------------------------------------
	-- Procesamos la columna 3.
	----------------------------------------------------------
	OPEN cursor_3 FOR SELECT * FROM clientes_temp_3;
	LOOP
		FETCH cursor_3 INTO reg_3;
		EXIT WHEN NOT FOUND;

		-- Si no esta en la columna 2 lo inserto.
		IF reg_3.nro_cli NOT IN (SELECT cod_cliente_sis_3 FROM "Equivalencia_Clientes" WHERE cod_cliente_sis_3 IS NOT NULL) THEN
			INSERT INTO "Equivalencia_Clientes" (cod_cliente_sis_3) VALUES (reg_3.nro_cli);
		END IF;

		-- Si esta en la tabla 1 lo inserto en la columna 1.
		OPEN cursor_1 FOR SELECT * FROM clientes_temp_1;
		LOOP
			FETCH cursor_1 INTO reg_1;
			EXIT WHEN NOT FOUND;

			IF reg_3.nombre SIMILAR TO reg_1.nombre THEN
				IF reg_3.direccion SIMILAR TO reg_1.direccion THEN
					UPDATE "Equivalencia_Clientes"
					SET cod_cliente_sis_1 = reg_1.nro_cli
					WHERE cod_cliente_sis_3 = reg_3.nro_cli;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_1;

		-- Si esta en la tabla 2 lo inserto en la columna 2.
		OPEN cursor_2 FOR SELECT * FROM clientes_temp_2;
		LOOP
			FETCH cursor_2 INTO reg_2;
			EXIT WHEN NOT FOUND;

			IF reg_3.nombre SIMILAR TO reg_2.nombre THEN
				IF reg_3.direccion SIMILAR TO reg_2.direccion THEN
					UPDATE "Equivalencia_Clientes"
					SET cod_cliente_sis_2 = reg_2.nro_cli
					WHERE cod_cliente_sis_3 = reg_3.nro_cli;
				END IF;
			END IF;
		END LOOP;
		CLOSE cursor_2;

	END LOOP;
	CLOSE cursor_3;

	DROP TABLE clientes_temp_1;
	DROP TABLE clientes_temp_2;
	DROP TABLE clientes_temp_3;

END;
$$ LANGUAGE plpgsql;

