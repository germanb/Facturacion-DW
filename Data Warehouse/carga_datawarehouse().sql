﻿-- CARGA DEL DATAWAREHOUSE
CREATE EXTENSION dblink;

-- Carga de las sucursales
INSERT INTO "Region" VALUES (1, 'Patagonia');
INSERT INTO "Region" VALUES (2, 'Cuyo');
INSERT INTO "Region" VALUES (3, 'Llanura');
INSERT INTO "Region" VALUES (4, 'Sierras');
INSERT INTO "Region" VALUES (5, 'Noroeste');
INSERT INTO "Region" VALUES (6, 'Meseta');
INSERT INTO "Provincia" VALUES (1, 'Neuquén', 1);
INSERT INTO "Provincia" VALUES (2, 'Rio Negro', 1);
INSERT INTO "Provincia" VALUES (3, 'Chubut', 1);
INSERT INTO "Provincia" VALUES (4, 'Santa Cruz', 1);
INSERT INTO "Ciudad" VALUES (1, 'Esquel', 3);
INSERT INTO "Ciudad" VALUES (2, 'Trelew', 3);
INSERT INTO "Ciudad" VALUES (3, 'Comodoro Rivadavia', 3);
INSERT INTO "Distribucion_Geografica" VALUES (1, 'Sucursal Esquel', 1);
INSERT INTO "Distribucion_Geografica" VALUES (2, 'Sucursal Trelew', 2);
INSERT INTO "Distribucion_Geografica" VALUES (3, 'Sucursal Cdoro. Riv.', 3);

-- Funcion para el llenado de tablas del DW
CREATE OR REPLACE FUNCTION carga_dw (sis_fact VARCHAR, fecha_desde DATE, fecha_hasta DATE) RETURNS void AS $$
DECLARE
	sucursal VARCHAR;
	nro_sucursal INT;
BEGIN	
	IF sis_fact SIMILAR TO 'Sistema_Facturacion_1' OR sis_fact SIMILAR TO 'Esquel' THEN
		sucursal := 'Sistema_Facturacion_1';
		nro_sucursal := 1;
	ELSE
		IF sis_fact SIMILAR TO 'Sistema_Facturacion_2' OR sis_fact SIMILAR TO 'Trelew' THEN
			sucursal := 'Sistema_Facturacion_2';
			nro_sucursal := 2;
		ELSE
			sucursal := 'Sistema_Facturacion_3';
			nro_sucursal := 3;
		END IF;
	END IF;

	-- Carga de las tablas de equivalencia.
	PERFORM carga_equivalencia_productos();
	PERFORM carga_equivalencia_clientes();

	-- Crear tabla temporal
	CREATE TEMP TABLE t (
		/*venta*/
		fecha_venta DATE,
		nro_factura INT,
		nro_cliente INT,
		nro_producto INT,
		forma_pago VARCHAR(30),
		monto REAL,
		cant INT,
		/*producto*/
		nombre_producto VARCHAR(30),
		categoria TEXT,
		/*cliente*/
		nombre_cliente VARCHAR(30),
		tipo_cliente TEXT
	);

	-- Conexion con la sucursal
	PERFORM dblink_connect('connection','dbname='||sucursal);

	-- Compone la consulta necesaria dependiendo del sitio.
	DECLARE
		consulta VARCHAR;
	BEGIN
		IF nro_sucursal = 1 THEN
			INSERT INTO t
		SELECT *
		FROM dblink('connection', 'SELECT v.fecha_venta, v.nro_factura, v.nro_cliente, dv. nro_producto, v.forma_pago,
					          dv.unidad * dv.precio as monto, dv.unidad,
					          p.nombre, (SELECT descripcion FROM "Categoria" WHERE nro_categoria = p.nro_categ),
					          c.nombre, c.tipo
				           FROM "Venta" as v, "Detalle_Venta" as dv, "Productos" as p, "Clientes" as c
				           WHERE dv.nro_factura = v.nro_factura AND
					         c.nro_cliente = v.nro_cliente AND
					         p.nro_producto = dv.nro_producto AND
					         v.fecha_venta BETWEEN ''' || fecha_desde || ''' AND ''' || fecha_hasta || ''';') 
	             as tabla(fecha_venta DATE, nro_factura INT, nro_cliente INT,
			      nro_producto INT, forma_pago VARCHAR(30), monto REAL, cant INT, nombre_producto VARCHAR(30),
			      categoria TEXT, nombre_cliente VARCHAR(30), tipo_cliente TEXT);
		ELSE
		-- Llenamos la tabla temporal.
		INSERT INTO t
		SELECT *
		FROM dblink('connection', 'SELECT v.fecha_vta, v.id_factura, v.cod_cliente, dv.cod_producto, mp.descripcion,
						dv.unidad * dv.precio as monto, dv.unidad,
						p.nombre, (SELECT ''descripción'' FROM "Categoria" WHERE cod_categoria = p.cod_categoria),
						c.nombre, (SELECT descripcion FROM "Tipo_Cliente" WHERE cod_tipo = c.cod_tipo)
					   FROM "Venta" as v, "Detalle_Venta" as dv, "Productos" as p, "Clientes" as c, "Medio_Pago" as mp
					   WHERE dv.id_factura = v.id_factura AND
	   				         c.cod_cliente = v.cod_cliente AND
					         p.cod_producto = dv.cod_producto AND
					         v.cod_medio_pago = mp.cod_medio_pago AND
					         v.fecha_vta BETWEEN ''' || fecha_desde || ''' AND ''' || fecha_hasta || ''';') 
		     as tabla(fecha_venta DATE, nro_factura INT, nro_cliente INT,
			      nro_producto INT, forma_pago VARCHAR(30), monto REAL, cant INT, nombre_producto VARCHAR(30),
			      categoria TEXT, nombre_cliente VARCHAR(30), tipo_cliente TEXT);
		END IF;
	END;

	-- LLenamos el DW.
	DECLARE
		curs refcursor;
		reg RECORD;
		t_cliente INT;
		cat INT;
		m_pago INT;
		tiempo INT;
		cliente_equiv INT;
		producto_equiv INT;

	BEGIN
		OPEN curs FOR SELECT * FROM t;
		LOOP
			FETCH curs INTO reg;
			EXIT WHEN NOT FOUND;
			
			m_pago := (SELECT actualiza_medios_pago(reg.forma_pago));
			tiempo := (SELECT actualiza_tiempo(reg.fecha_venta));

			EXECUTE('SELECT cod_cliente_equiv FROM "Equivalencia_Clientes" WHERE cod_cliente_sis_'||nro_sucursal||' = '||reg.nro_cliente||';') INTO cliente_equiv;
			EXECUTE('SELECT id_producto_equiv FROM "Equivalencia_Productos" WHERE id_producto_sis_'||nro_sucursal||' = '||reg.nro_producto||';') INTO producto_equiv;
			-- cliente_equiv := (SELECT cod_cliente_equiv FROM "Equivalencia_Clientes" WHERE 'cod_cliente_sis_'||nro_sucursal = reg.nro_cliente);
			-- producto_equiv := (SELECT id_producto_equiv FROM "Equivalencia_Productos" WHERE 'id_producto_sis_'||nro_sucursal = reg.nro_producto);
				
			IF (SELECT id_cliente FROM "Clientes" WHERE id_cliente = cliente_equiv) IS NULL THEN
				t_cliente := (SELECT actualiza_tipo_cliente(reg.tipo_cliente));
				
				INSERT INTO "Clientes" VALUES (cliente_equiv, reg.nombre_cliente, t_cliente);
			END IF;
			IF (SELECT id_producto FROM "Productos" WHERE id_producto = producto_equiv) IS NULL THEN
				cat := (SELECT actualiza_categoria_producto(reg.categoria));
				
				INSERT INTO "Productos" VALUES (producto_equiv, reg.nombre_producto, cat);
			END IF;
			-- Crea la venta.
			INSERT INTO "Ventas" VALUES (reg.fecha_venta, reg.nro_factura, cliente_equiv, producto_equiv, 
						     nro_sucursal, m_pago, tiempo, reg.monto, reg.cant);
		END LOOP;
	CLOSE curs;
	END;
	
	DROP TABLE t;
	PERFORM dblink_disconnect('connection');

END;
$$ LANGUAGE plpgsql;

SELECT carga_dw('Esquel','2009-01-01','2009-01-30');

SELECT carga_dw('Trelew','2011-01-01','2011-01-30');

SELECT dblink_disconnect('connection');

